
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity block_tb is
--  Port ( );
end block_tb;

architecture Behavioral of block_tb is


signal Clk: std_logic:='0';
signal Rst: std_logic := '0';
constant CLK_PERIOD : TIME := 10 ns;
signal wea : STD_LOGIC_VECTOR(0 DOWNTO 0):="0";

signal addra : STD_LOGIC_VECTOR(16 DOWNTO 0):=(others=>'0');
signal dina : STD_LOGIC_VECTOR(15 DOWNTO 0):=(others=>'0');
signal douta : STD_LOGIC_VECTOR(15 DOWNTO 0):=(others=>'0');


signal obstacleBlock: std_logic_vector( 2000 downto 0):=(others=>'0');
	signal pixel:std_logic_vector(11 downto 0);

signal s_hpos:integer:=0;
signal s_vpos:integer:=0;

	signal s_VGARed   : std_logic_vector(3 downto 0);
	signal s_VGAGreen : std_logic_vector(3 downto 0);
	signal s_VGABlue  : std_logic_vector(3 downto 0);

    signal addrb : STD_LOGIC_VECTOR(16 DOWNTO 0):=(others=>'0');
    signal dim_img:integer:=300;
    	signal center_x:integer:=800/2;
	   signal center_y:integer:=600/2;
	   
	   signal block_number:integer;
begin

blocuri:process
begin

for i in 250 to 550 loop
s_hpos<=i;
for j in 150 to 450 loop
s_vpos<=j;

wait for 10ns;
end loop;
end loop;
wait;
end process blocuri;
addrB<=STD_LOGIC_VECTOR(to_unsigned(( (s_vpos-(center_y-dim_img/2))*dim_img) + (s_hpos-(center_x-dim_img/2)),17));
block_number<=((300/20) * ((s_hpos-250)/20) + ((s_vpos-150)/20)); 
Registru3:entity WORK.blk_mem_gen_0
  PORT map(
    clka=>clk,
    ena=>'1',
    wea=>wea,
    addra=>addrb,
    dina=>dina,
    douta=>douta
  );
  
  print_imagine: entity WORK.print_image
port map(clk=>clk,
        rst=>'0',
        dim_x=>800,
        dim_y=>600,
        h_pos=>s_hpos,
        v_pos=>s_vpos,
        videoOn=>'1',
        douta=>douta,
        red=>s_vgared,
        green=>s_vgagreen,
        blue=>s_vgablue,
        addr=>addra
);

  
  
pixel<=douta(15 downto 4);
blocuri_obstacole:entity WORK.create_blocks
port map(
        clk=>clk,
        en=>'1',
        hpos=>s_hpos,
        vpos=>s_vpos,
        pixel=>pixel,
        obstacle=>obstacleBlock
);


 gen_clk:process
 begin
 clk<='1';
 wait for CLK_PERIOD/2;
 clk<='0';
 wait for CLK_PERIOD/2;
 end process gen_clk;
 

end Behavioral;
