
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_arith.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity vga_driver is
generic(dim_player:integer:=20);
	Port(
		p_Clock    : in  std_logic;
		p_Reset    : in  std_logic;
		p_Color    : in  std_logic_vector(2 downto 0);
		direction  : in  std_logic_vector(3 downto 0);
		o_HPos     : out integer;
		o_VPos     : out integer;
		o_VGAHS    : out std_logic;
		o_VGAVS    : out std_logic;
		o_VGARed   : out std_logic_vector(3 downto 0);
		o_VGAGreen : out std_logic_vector(3 downto 0);
		o_VGABlue  : out std_logic_vector(3 downto 0)
	);
end vga_driver;

architecture Behavioral of vga_driver is



subtype word_t  is std_logic_vector(11 downto 0);
type    registru   is array(0 to 480000) of word_t;


subtype word_t2  is std_logic_vector(23 downto 0);
type    registru2   is array(0 to 2) of word_t2;

impure function readFromFile(FileName:STRING) return registru is

  variable cnt:integer:=0;
    variable l: line;
    file f: text is in FileName;
    variable i: integer;
    variable good: boolean;
    variable by:std_logic_Vector(7 downto 0);
    
    variable r:std_logic_Vector(7 downto 0);
    variable g:std_logic_Vector(7 downto 0);
    variable b:std_logic_Vector(7 downto 0);
    variable cnt_rgb:integer;
variable Result: registru    := (others => (others => '0'));
variable result2:word_t2;

begin

   

  loop
    cnt_rgb:=0;
    readline(f, l); 

      hread(l, result2);
   
      r:=result2(23 downto 16);
      g:=result2(15 downto 8);
      b:=result2(7 downto 0);
      
   
    Result(cnt):=r(7 downto 4) & g(7 downto 4) & b(7 downto 4);
    
    cnt:=cnt+1;
    exit when cnt=480;
    end loop;
    
    
return Result;

end function;

--signal ram_mem    : registru    := readFromFile("deleteme.txt");



	-- Timing Constants
	-- For polarity '0' means negative polarity

	-- Horizontal Axis
	constant HD        : integer   := 800; -- Visiable Area
	constant HFP       : integer   := 40; -- Front Porch
	constant HSP       : integer   := 128; -- Sync Pulse
	constant HBP       : integer   := 88; -- Back porch
	constant HPOLARITY : std_logic := '1'; -- Polartity
	constant HTOTAL    : integer   := HD + HFP + HSP + HBP; -- Whole Line

	-- Vertical Axis
	constant VD        : integer   := 600; -- Visiable Area
	constant VFP       : integer   := 1; -- Front Porch
	constant VSP       : integer   := 4; -- Sync Pulse
	constant VBP       : integer   := 23; -- Back porch
	constant VPOLARITY : std_logic := '1'; -- Polartity
	constant VTOTAL    : integer   := VD + VFP + VSP + VBP; -- Whole Line

	signal s_HPos     : integer;
	signal s_VPos     : integer;
	signal s_videoOn  : std_logic;
	signal s_VGARed   : std_logic_vector(3 downto 0);
	signal s_VGAGreen : std_logic_vector(3 downto 0);
	signal s_VGABlue  : std_logic_vector(3 downto 0);
	
	
	signal red_aux:std_logic_vector(3 downto 0);
	signal green_aux:std_logic_vector(3 downto 0);
	signal blue_aux:std_logic_vector(3 downto 0);
	
	signal wea : STD_LOGIC_VECTOR(0 DOWNTO 0):="0";
    signal addra : STD_LOGIC_VECTOR(16 DOWNTO 0):=(others=>'0');
    signal dina : STD_LOGIC_VECTOR(15 DOWNTO 0):=(others=>'0');
    signal douta : STD_LOGIC_VECTOR(15 DOWNTO 0):=(others=>'0');
    constant picture_size : Integer:=90000;
    
    
    signal player_color_red:std_logic_vector(3 downto 0);
	signal player_color_green:std_logic_vector(3 downto 0);
	signal player_color_blue:std_logic_vector(3 downto 0);
	signal drawPlayer:std_logic:='0';
	
	signal player_x : INTEGER := 400;
	signal player_y : INTEGER := 400;
	
	signal next_player_x:integer;
	signal next_player_y:integer;
	
	signal enObstacle:std_logic:='1';
	signal obstacleBlock: std_logic;
	signal pixel:std_logic_vector(11 downto 0);
	
	signal block_number:integer;
    signal obstacle_signal:std_logic_Vector(2000 downto 0):=(others=>'0');
	
	
	signal center_x:integer:=400;
signal center_y:integer:=300;
signal dim_img:integer:=300;


begin



Registru3:entity WORK.blk_mem_gen_0
  PORT map(
    clka=>p_clock,
    ena=>'1',
    wea=>wea,
    addra=>addra,
    dina=>dina,
    douta=>douta
  );


  red_aux<=douta(15 downto 12);
  green_aux<=douta(11 downto 8);
  blue_Aux<=douta(7 downto 4);



	-- Horizontal Position Counter
	HPCounter : process(p_Clock, p_Reset)
	begin
		if (p_Reset = '1') then
			s_HPos <= 0;
		elsif (rising_edge(p_Clock)) then
			if (s_HPos = HTOTAL) then
				s_HPos <= 0;
			else
				s_HPos <= s_HPos + 1;
			end if;
		end if;
	end process;
	o_HPos <= s_HPos;

	-- Vertical Position Counter
	VPCounter : process(p_Clock, p_Reset)
	begin
		if (p_Reset = '1') then
			s_VPos <= 0;
		elsif (rising_edge(p_Clock)) then
			if (s_HPos = HTOTAL) then
				if (s_VPos = VTOTAL) then
					s_VPos <= 0;
				else
					s_VPos <= s_VPos + 1;
				end if;
			end if;
		end if;
	end process;
	o_VPos <= s_VPos;

	-- Horizontal Synchronisation
	HSync : process(p_Clock, p_Reset)
	begin
		if (p_Reset = '1') then
			o_VGAHS <= HPOLARITY;
		elsif (rising_edge(p_Clock)) then
			if ((s_HPos < HD + HFP) OR (s_HPos > HD + HFP + HSP)) then
				o_VGAHS <= not HPOLARITY;
			else
				o_VGAHS <= HPOLARITY;
			end if;
		end if;
	end process;

	-- Vertical Synchronisation
	VSync : process(p_Clock, p_Reset)
	begin
		if (p_Reset = '1') then
			o_VGAVS <= VPOLARITY;
		elsif (rising_edge(p_Clock)) then
			if ((s_VPos < VD + VFP) OR (s_VPos > VD + VFP + VSP)) then
				o_VGAVS <= not VPOLARITY;
			else
				o_VGAVS <= VPOLARITY;
			end if;
		end if;
	end process;

	-- Video On
	videoOn : process(p_Clock, p_Reset)
	begin
		if (p_Reset = '1') then
			s_videoOn <= '0';
		elsif (rising_edge(p_Clock)) then
			if (s_HPos <= HD and s_VPos <= VD) then
				s_videoOn <= '1';
			else
				s_videoOn <= '0';
			end if;
		end if;
	end process;

print_imagine: entity WORK.print_image
port map(clk=>p_clock,
        rst=>p_reset,
        dim_x=>HD,
        dim_y=>VD,
        h_pos=>s_hpos,
        v_pos=>s_vpos,
        videoOn=>s_videoOn,
        douta=>douta,
        red=>s_vgared,
        green=>s_vgagreen,
        blue=>s_vgablue,
        addr=>addra
);

draw_player:entity WORK.print_player
port map(clk=>p_clock,
        poz_x=>next_player_x,
        poz_y=>next_player_y,
        h_pos=>s_hpos,
        v_pos=>s_vpos,
        red=>player_color_red,
        green=>player_color_green,
        blue=>player_color_blue,
        drawPlayer=>drawPlayer
        );


     
pixel<=douta(15 downto 4);
blocuri_obstacole:entity WORK.create_blocks
port map(
        clk=>p_clock,
        en=>'1',
        hpos=>s_hpos,
        vpos=>s_vpos,
        pixel=>pixel,
        obstacle=>obstacleBlock
);

 block_number<=((300/20) * ((s_hpos-250)/20) + ((s_vpos-150)/20)); 
 
 Obstacles:process(p_clock)
 begin
 
 if(s_hpos<(center_x+dim_img/2) and s_hpos>(center_x-dim_img/2)
			      and s_vpos<(center_y+dim_img/2) and s_vpos>(center_y-dim_img/2)) then
			      
			      if(obstacleBlock='1') then
			         obstacle_signal(block_number)<=obstacleBlock;
			      end if;
 
 end if;
 
 end process Obstacles;
 
move_player: entity WORK.move port map(
        obstacle=>obstacle_signal,
        Clk => p_clock,
        cur_x => player_x,
        cur_y => player_y,
        up => direction(3),
        down => direction(2),
        left => direction(1),
        right => direction(0),
        next_x => next_player_x,
        next_y => next_player_y
);

	o_VGARed   <= s_VGARed when drawPlayer='0' else player_color_red;
	o_VGAGreen <= s_VGAGreen when drawPlayer='0' else player_color_green;
	o_VGABlue  <= s_VGABlue when drawPlayer='0' else player_color_blue;

end Behavioral;
