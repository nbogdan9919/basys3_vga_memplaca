library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity main is
	Port(
		CLOCK     : in  std_logic;
		RESET     : in  std_logic;
		RX        : in  std_logic;
		
		up        : in std_logic;
		left      : in std_logic;
		down      : in std_logic;
		right     : in std_logic;
		
		ANODE     : out std_logic_vector(3 downto 0);
		CATHODE   : out std_logic_vector(6 downto 0);
		VGA_VS    : out std_logic;
		VGA_HS    : out std_logic;
		VGA_RED   : out std_logic_vector(3 downto 0);
		VGA_BLUE  : out std_logic_vector(3 downto 0);
		VGA_GREEN : out std_logic_vector(3 downto 0)
	);
end main;

architecture Behavioral of main is

component clock_wizard
		port(
			vga_clock : out std_logic;
			reset     : in  std_logic;
			locked    : out std_logic;
			clock_in  : in  std_logic
		);
	end component;

	component vga_driver is
		Port(
			p_Clock    : in  std_logic;
			p_Reset    : in  std_logic;
			p_Color    : in  std_logic_vector(2 downto 0);
			direction  : in  std_logic_vector(3 downto 0);
			o_HPos     : out integer;
			o_VPos     : out integer;
			o_VGAHS    : out std_logic;
			o_VGAVS    : out std_logic;
			o_VGARed   : out std_logic_vector(3 downto 0);
			o_VGAGreen : out std_logic_vector(3 downto 0);
			o_VGABlue  : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Constants
	constant c_CLOCK_FREQ : integer := 40_000_000;
	constant c_BAUD_RATE  : integer := 9600;

	-- Clock Driver Output
	signal s_GameClock : std_logic;

	-- VGA Clock
	signal s_VGAClock : std_logic;
	signal s_Locked   : std_logic;
	signal s_Reset    : std_logic;

	-- Entity Controller Output
	signal s_Color     : std_logic_vector(2 downto 0);
	signal s_Score     : std_logic_vector(15 downto 0);

	-- VGA Driver Output
	signal s_HPos : integer := 0;
	signal s_VPos : integer := 0;

	-- IO
	signal s_Move      : std_logic;
	signal s_Direction : std_logic_vector(3 downto 0);
        
    signal d_up        : std_logic;
    signal d_down      : std_logic;
    signal d_left      : std_logic;
    signal d_right     : std_logic;
    
    signal direction   : std_logic_vector(3 downto 0);  
    
    signal obstacle_in:std_logic_Vector(2000 downto 0):=(others=>'0');
    signal obstacle_out:std_logic_Vector(2000 downto 0);

begin

my_wizard : clock_wizard
		port map(
			vga_clock => s_VGAClock,
			reset     => RESET,
			locked    => s_Locked,
			clock_in  => CLOCK
		);
	s_Reset <= not s_Locked;

	my_vga : vga_driver
		Port map(
			p_Clock    => s_VGAClock,
			p_Reset    => s_Reset,
			p_Color    => s_Color,
			direction  => direction,
			o_HPos     => s_HPos,
			o_VPos     => s_VPos,
			o_VGAHS    => VGA_HS,
			o_VGAVS    => VGA_VS,
			o_VGARed   => VGA_RED,
			o_VGAGreen => VGA_GREEN,
			o_VGABlue  => VGA_BLUE
		);

direction <= d_up & d_down & d_left & d_right;


debounce_left: entity WORK.debounce port map(
    Clk => s_VGAClock,
    Rst => RESET, 
    Din => left,
    Qout => d_left
);

debounce_right: entity WORK.debounce port map(
    Clk => s_VGAClock,
    Rst => RESET, 
    Din => right,
    Qout => d_right
);

debounce_up: entity WORK.debounce port map(
    Clk => s_VGAClock,
    Rst => RESET, 
    Din => up,
    Qout => d_up
);

debounce_down: entity WORK.debounce port map(
    Clk => s_VGAClock,
    Rst => RESET, 
    Din => down,
    Qout => d_down
);
end Behavioral;
