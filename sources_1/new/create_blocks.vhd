
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.NUMERIC_STD.ALL;

entity create_blocks is
generic(dim_player:integer:=20);
  Port(
        clk: in std_logic;
        en:in std_logic;
        hpos:in integer;
        vpos:in integer;
        pixel:in std_logic_vector(11 downto 0);
        obstacle:out std_logic
         );
end create_blocks;

architecture Behavioral of create_blocks is

signal block_number:integer;

signal obstacle_signal: std_logic;

signal center_x:integer:=400;
signal center_y:integer:=300;
signal dim_img:integer:=300;

begin

genereaza_blocuri: process(clk,en,pixel)
begin

if(rising_edge(clk)) then
    
        if(hpos<(center_x+dim_img/2) and hpos>(center_x-dim_img/2)
			      and vpos<(center_y+dim_img/2) and vpos>(center_y-dim_img/2)) then
			      
            block_number<=((300/20) * ((hpos-250)/20) + ((vpos-150)/20));       
                if(pixel=x"000") then
                    obstacle_signal<='1';
        end if;

    end if;

end if;

end process genereaza_blocuri;

obstacle<=obstacle_signal;


end Behavioral;
