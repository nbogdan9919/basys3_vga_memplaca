
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity print_image is
generic(dim_img:integer:=300);
  Port (
        clk:in std_logic;
        rst:in std_logic;
        dim_x:in integer;
        dim_y:in integer;
        h_pos:in integer;
        v_pos:in integer;
        videoOn:in std_logic;
        douta:in std_logic_vector(15 downto 0);
        red:out std_logic_vector(3 downto 0);
        green:out std_logic_vector(3 downto 0);
        blue:out std_logic_vector(3 downto 0);
        addr:out std_logic_vector(16 downto 0)
        );
end print_image;

architecture Behavioral of print_image is

	signal red_aux:std_logic_vector(3 downto 0);
	signal green_aux:std_logic_vector(3 downto 0);
	signal blue_aux:std_logic_vector(3 downto 0);
	
	signal center_x:integer:=dim_x/2;
	signal center_y:integer:=dim_y/2;
	
	signal addr_Sign:std_logic_Vector(16 downto 0);
	
begin

  red_aux<=douta(15 downto 12);
  green_aux<=douta(11 downto 8);
  blue_Aux<=douta(7 downto 4);


	colorOutput : process(clk, rst,h_pos,v_pos,videoOn)
	begin
		if (rst = '1') then
			red   <= (others => '0');
			green <= (others => '0');
			blue  <= (others => '0');
			
		elsif (rising_edge(clk)) then
		
		      if(h_pos=799 and v_pos=599) then
		          addr_sign<=(others=>'0');
		      end if;
		
		
		
			if (VideoOn = '1') then
			     if(h_pos<(center_x+dim_img/2) and h_pos>(center_x-dim_img/2)
			      and v_pos<(center_y+dim_img/2) and v_pos>(center_y-dim_img/2)) then
	
				    red   <= red_aux;
				    green <= green_Aux;
			        blue <= blue_aux;			         

			         addr_sign<=STD_LOGIC_VECTOR(to_unsigned(( (v_pos-(center_y-dim_img/2))*dim_img) + (h_pos-(center_x-dim_img/2)),17));
			         
			         else
			             
			            red   <= (others => '0');
				        green <= (others => '0');
				        blue  <= (others => '0');
				        addr_sign<=(others=>'0');
			     end if;
			     
			else
			     
				red   <= (others => '0');
				green <= (others => '0');
				blue  <= (others => '0');
				addr_Sign<=(others=>'0');
			end if;
			
			
		  
		end if;
	end process;
	addr<=addr_sign;
end Behavioral;
