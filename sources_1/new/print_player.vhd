
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity print_player is
 generic(dim:integer:=20);
  Port (
        clk:in std_logic;
        poz_x:in integer;
        poz_y:in integer;
        h_pos:in integer;
        v_pos:in integer;
        red:out std_logic_vector(3 downto 0);
        green:out std_logic_Vector(3 downto 0);
        blue:out std_logic_vector(3 downto 0);
        drawPlayer:out std_logic
     );
end print_player;

architecture Behavioral of print_player is

signal colt_stanga_sus_x:integer:=poz_x - dim/2;
signal colt_stanga_sus_y:integer:=poz_y - dim/2;

signal colt_dreapta_sus_x:integer:=poz_x + dim/2;
signal colt_dreapta_sus_y:integer:=poz_y - dim/2;

signal colt_stanga_jos_x:integer:=poz_x - dim/2;
signal colt_stanga_jos_y:integer:=poz_y + dim/2;

signal colt_dreapta_jos_x:integer:=poz_x + dim/2;
signal colt_dreapta_jos_y:integer:=poz_y + dim/2;

signal drawSignal:std_logic;

signal addra : STD_LOGIC_VECTOR ( 8 downto 0 ) := (others => '0');
signal dina  : STD_LOGIC_VECTOR ( 15 downto 0 );
signal douta : STD_LOGIC_VECTOR ( 15 downto 0 );

signal s_red : std_logic_vector(3 downto 0);
signal s_blue: std_logic_vector(3 downto 0);
signal s_green : std_logic_vector(3 downto 0);


signal x_poza_player : INTEGER := 0;
signal y_poza_player : INTEGER := 0;


begin

img_player: entity WORK.player_mem port map(
    clka=>clk,
    ena=>'1',
    wea=>"0",
    addra=>addra,
    dina=>dina,
    douta=>douta
);

draw:process(clk)
begin
    if(rising_edge(clk)) then
        if( h_pos>=colt_stanga_sus_x and h_pos<=colt_dreapta_sus_x
        and v_pos>=colt_stanga_sus_y and v_pos<=colt_stanga_jos_y) then
            drawSignal<='1';
            x_poza_player <= h_pos - colt_stanga_sus_x; 
            y_poza_player <= v_pos - colt_stanga_sus_y; 
            addra <= STD_LOGIC_VECTOR(to_unsigned(y_poza_player * 20 + x_poza_player, 9));
        else drawSignal<='0';
        end if;
    end if;
end process draw;




red<= douta(15 downto 12) when drawSignal = '1' else "1000";
green<= douta(11 downto 8) when drawSignal = '1' else "1000";
blue<= douta(7 downto 4) when drawSignal = '1' else "1000";


drawPlayer<=drawSignal;

end Behavioral;