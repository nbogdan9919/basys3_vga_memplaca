
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity move is
    generic(move_unit:integer:=10;dim_player:integer:=20);
    
    Port (
        obstacle:in std_logic_vector( 2000 downto 0);
        clk:in std_logic;
        cur_x:in integer;
        cur_y:in integer;
        up:   in std_logic;
        down: in std_logic;
        left: in std_logic;
        right:in std_logic;
        next_x:out integer;
        next_y:out integer
        
   );
end move;

architecture Behavioral of move is

signal decision:std_logic_vector(3 downto 0);

signal next_x_signal:integer;
signal next_y_signal:integer;

constant MAX_X : INTEGER := 300;
constant MAX_Y : INTEGER := 300;

constant MIN_X : INTEGER := 0;
constant MIN_Y : INTEGER := 0;


signal block_number:integer;


signal center_x:integer:=400;
signal center_y:integer:=300;
signal dim_img:integer:=300;

begin

decision<=up & down & left & right;




move_decision:process(clk)
begin
    if rising_edge(clk) then
        case decision is
        
            when "0000" =>            -- no moves
                next_x_signal <= cur_x;
                next_y_signal <= cur_y;
            
            when "0001" =>           --move right
                --if(cur_x + move_unit < MAX_X) then
                    next_x_signal <= cur_x + move_unit;
                    next_y_signal <= cur_y;
                --end if;
                
            when "0010" =>          --move left
                --if(cur_x - move_unit > MIN_X) then
                    next_x_signal <= cur_x - move_unit;
                    next_y_signal <= cur_y;
                --end if;
                
            when "0100" =>      --move down
                --if(cur_y + move_unit < MAX_Y) then
                    next_x_signal <= cur_x; 
                    next_y_signal <= cur_y + move_unit;
                --end if;
            
            when "1000" =>     --move up
                --if(cur_y - move_unit > MIN_Y) then
                    next_x_signal <= cur_x;
                    next_y_signal <= cur_y - move_unit;
                --end if;
                           
            when others =>
                next_x_signal <= cur_x;
                next_y_signal <= cur_y;
            
            end case;
            
            
    block_number<=((300/20) * ((next_x_signal-250)/20) + ((next_y_signal-150)/20));  
   -- if(obstacle(block_number)='1') then
     --   next_x_signal<=cur_x;
       -- next_y_signal<=cur_y;    
     --end if;  
     
 end if;
        

end process move_decision;

next_x <= next_x_signal;
next_y <= next_y_signal;


end Behavioral;