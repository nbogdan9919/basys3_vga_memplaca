
library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity read_image is
  Port (
  load:in std_logic;
  x:in integer;
  y:in integer;
  red:out std_logic_vector(3 downto 0);
  green:out std_logic_vector(3 downto 0);
  blue: out std_logic_vector(3 downto 0)
  
  
  
   );
end read_image;

architecture Behavioral of read_image is



subtype word_t  is std_logic_vector(11 downto 0);
type    registru   is array(0 to 480) of word_t;


subtype word_t2  is std_logic_vector(23 downto 0);
type    registru2   is array(0 to 2) of word_t2;

impure function readFromFile(FileName:STRING) return registru is

  variable cnt:integer:=0;
    variable l: line;
    file f: text is in FileName;
    variable i: integer;
    variable good: boolean;
    variable by:std_logic_Vector(7 downto 0);
    
    variable r:std_logic_Vector(7 downto 0);
    variable g:std_logic_Vector(7 downto 0);
    variable b:std_logic_Vector(7 downto 0);
    variable cnt_rgb:integer;
variable Result: registru    := (others => (others => '0'));
variable result2:word_t2;

begin

   

  loop
    cnt_rgb:=0;
    readline(f, l); 

      hread(l, result2);
   
      r:=result2(23 downto 16);
      g:=result2(15 downto 8);
      b:=result2(7 downto 0);
      
   
    Result(cnt):=r(7 downto 4) & g(7 downto 4) & b(7 downto 4);
    
    cnt:=cnt+1;
    exit when cnt=480;
    end loop;
    
    
return Result;

end function;

signal ram_mem    : registru    := readFromFile("deleteme.txt");

begin

  
  red<=ram_mem(x*800+y)(11 downto 8);
  green<=ram_mem(x*800+y)(7 downto 4);
  blue<=ram_mem(x*800+y)(3 downto 0);


end Behavioral;
